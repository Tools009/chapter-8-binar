import './App.css';
import { useState, Fragment } from "react";
import data from "./mock-data.json";
import TableRow from './component/TableRow';
import EditTable from './component/EditTable';



function App() {

  const [contacts, setContacts] = useState(data);
  const [addFormData, setAddFormData] = useState({
    id: '',
    Username: '',
    Email: '',
    Experience: '',
    Level: ''
  });

  const [editFormData, setEditFormData] = useState({
    id: '',
    Username: '',
    Email: '',
    Experience: '',
    Level: ''

  });

  const [editContactId, setEditContactId] = useState(null)

  const handleAddFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute('name');
    const fieldValue = event.target.value;

    const newFormData = {...addFormData};
    newFormData[fieldName] = fieldValue;

    setAddFormData(newFormData);

  };

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute('name');
    const fieldValue = event.target.value;

    const newFormData = {...editFormData};
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData)
  }

  const handleAddFormSubmit = (event) => {
    event.preventDefault();

    const newContact = {
      id: addFormData.id,
      Username: addFormData.Username,
      Email: addFormData.Email,
      Experience: addFormData.Experience,
      Level: addFormData.Level
    };

    const newContacts = [...contacts, newContact];
    setContacts(newContacts);

  };

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editedContact = {
      id: editFormData.id,
      Username: editFormData.Username,
      Email: editFormData.Email,
      Experience: editFormData.Experience,
      Level: editFormData.Level,
    };

    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) =>
      contact.id === editContactId);

    newContacts[index] = editedContact

    setContacts(newContacts)
    setEditContactId(null)
  }

  const handleEditClick = (event, contact) => {
    event.preventDefault();
    setEditContactId(contact.id);

    const formValues = {
      id : contact.id,
      Username : contact.Username,
      Email : contact.Email,
      Experience : contact.Experience,
      Level: contact.Level,
    }

    setEditFormData(formValues)

  }

  const handleCanceledClick = () => {
    setEditContactId(null)
  }

  const handleDeleteClick = (contactId) => {
    const newContacts = [...contacts];

    const index = contacts.findIndex((contacts) =>
    contacts.id === contactId)

    newContacts.splice(index, 1)
    setContacts(newContacts)
  }

  return (
    
    <div className="App">
      <form onSubmit={handleEditFormSubmit} >
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Experience</th>
            <th>Level</th>
            <th>Action</th>
          </tr>
        </thead>
      <tbody>
        {contacts.map((contact) => (

        <Fragment>
        
          { editContactId === contact.id ? 
          (<EditTable editFormData={editFormData} 
            handleEditFormChange={handleEditFormChange}
            handleCanceledClick={handleCanceledClick  }/>) : 
          (<TableRow contact = { contact } 
          handleEditClick={handleEditClick} 
          handleDeleteClick={handleDeleteClick}/>)  }
          
        </Fragment>     
        ))}
      </tbody>
      </table>
      </form>
      
      <br></br>

      <h3>Add Player</h3>

      <br></br>
      
      <form onSubmit={handleAddFormSubmit}>

      <input className='addHandler1'
          type="number"
          name="id"
          required="required"
          placeholder="Enter a id"
          onChange = {handleAddFormChange}>
          
          </input>
        
        <input className='addHandler1'
          type="text"
          name="Username"
          required="required"
          placeholder="Enter a Username"
          onChange = {handleAddFormChange}>
          
          </input>

          <input className='addHandler2'
          type="email"
          name="Email"
          required="required"
          placeholder="Enter a Email"
          onChange = {handleAddFormChange}>
          
          </input>

          <input className='addHandler3'
          type="number"
          name="Experience"
          required="required"
          placeholder="Enter a Experrience"
          onChange= {handleAddFormChange}>
          
          </input>

          <input className='addHandler4'
          type="number"
          name="Level"
          required="required"
          placeholder="Enter a Level"
          onChange= {handleAddFormChange}>
          
          </input>
          
          
          <button type="submit">Add</button>

      </form>
  
    </div>
  );
}

export default App;
