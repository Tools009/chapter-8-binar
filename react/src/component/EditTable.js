import React from 'react'

const EditTable = ({editFormData, handleEditFormChange, handleCanceledClick}) => {
  return (
    <tr>
        <td>
            <input
            type= "number"
            required="required"
            placeholder="enter an id"
            name="id"
            value={editFormData.id}
            onChange={handleEditFormChange}>
            </input>

        </td>

        <td>
            <input
            type= "text"
            required="required"
            placeholder="enter an Username"
            name="Username"
            defaultValue={editFormData.Username}
            onChange={handleEditFormChange}>
            </input>

        </td>

        <td>
            <input
            type= "email"
            id='email'
            required="required"
            placeholder="enter an Email"
            name="Email"
            defaultValue={editFormData.Email}
            onChange={handleEditFormChange}>
            </input>

        </td>

        <td>
            <input
            type= "number"
            required="required"
            placeholder="enter an Experience"
            name="Experience"
            defaultValue={editFormData.Experience}
            onChange={handleEditFormChange}>
            </input>

        </td>

        <td>
            <input
            type= "number"
            required="required"
            placeholder="enter an Level"
            name="Level"
            defaultValue={editFormData.Level}
            onChange={handleEditFormChange}>
        
            </input>

        </td>

        <td>
            <button type="submit">save</button>
            <button type='button' onClick={handleCanceledClick}>cancel</button>
        </td>

    </tr>
  )
}

export default EditTable