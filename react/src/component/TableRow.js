import React from 'react'

const TableRow = ( {contact , handleEditClick, handleDeleteClick } ) => {
  return (

    <tr>
    <td>{contact.id}</td>
    <td>{contact.Username}</td>
    <td>{contact.Email}</td>
    <td>{contact.Experience}</td>
    <td>{contact.Level}</td>
    <td>
      <button type="button" 
      onClick={(event) => handleEditClick(event, contact)}>edit</button>
      <button type="button"
      onClick={(event) => handleDeleteClick(contact.id)}>Delete</button>
    </td>

</tr>
  )
}

export default TableRow