const PlayerController = require("../../controllers/player.controller");
const playerRouter = require("express").Router();

/**
 * @Routes "/api/v1/players"
 */

/**
 * @swagger
 * components:
 *  schemas:
 *    Players:
 *      type: object
 *      required:
 *      - id
 *      - username
 *      - email
 *      - password
 *      - experience
 *      - lvl 
 *      properties:
 *        id:
 *         type : string
 *         description: for generated id players
 *        username:
 *          type: string
 *          description: for username 
 *        password :
 *          type: string
 *          description: for password 
 *        experience :
 *          type: integer
 *          description: experience player
 *        lvl :
 *          type: integer
 *          description: based on experience
 *           
 */

/**
 * @swagger
 * /api/v1/players/ :
 *  get:
 *      summary: Return list of players that play the game
 *      tags: [Players]
 *      responses:
 *          200:
 *              description: list of players that play the game
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/Players'
 */

/**


/**
 * @swagger
 * /api/v1/players/ :
 *  post:
 *      summary: for adding to the list of players
 *      tags: [Players]
 *      responses:
 *          201:
 *              description : for adding  add a list of players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          
 */

/**
 * @swagger
 * /api/v1/players/{id} :
 *  get:          
 *      summary: Show players by each id 
 *      tags: [Players]
 *      parameters:
 *          -   in : path
 *              name: id
 *              schema: 
 *                  type: string
 *              required: true
 *              description: Show players by id
 *      responses:
 *          201:
 *              description : show players by id
 *              content: 
 *                  application/json:
 *                      schema:
 *                              $ref: '#/components/schemas/Players'
 *          404: 
 *              description: Players was not found in database
 *          
 */
/**
 * @swagger
 * /api/v1/players/{id} :
 *  put:          
 *      summary: For updating players by id 
 *      tags: [Players]
 *      parameters:
 *          -   in : path
 *              name: id
 *              schema: 
 *                  type: string
 *              required: true
 *              description: players id
 *      responses:
 *          201:
 *              description : fpr updating players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: the players id not found 
 */

/**
 * @swagger
 * /api/v1/players/{id} :
 *  delete:
 *      summary : idd you want to delete
 *      tags: [Players]
 *      parameters: 
 *          -   in : path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: players id
 *      responses:
 *          201:
 *              description : delete players
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: players id not found  
 */

/**
 * @swagger
 * /api/v1/players/exp/{id} :
 *  post:
 *      summary : id you want update experience
 *      tags: [Players]
 *      parameters: 
 *          -   in : path
 *              name: id
 *              schema:
 *                  type: string
 *              required: true
 *              description: players id
 *      responses:
 *          201:
 *              description : update experience id
 *              content: 
 *                  application/json:
 *                      schema:
 *                             
 *                          items:
 *                              $ref: '#/components/schemas/Players'
 *          404:
 *              description: players id not found  
 */

playerRouter.get("/", PlayerController.getPlayers);
playerRouter.post("/", PlayerController.createPlayer);
playerRouter.get("/:id", PlayerController.getPlayerById);
playerRouter.put("/:id", PlayerController.updatePlayer);
playerRouter.delete("/:id", PlayerController.deletePlayer);
playerRouter.post("/exp/:id", PlayerController.updateExperience);

module.exports = playerRouter;
